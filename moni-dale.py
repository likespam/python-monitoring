import os, sys, platform, socket, datetime, psutil as psu, logging

#Variabelen für die Funktionen, damit das Log sie ausgeben kann
zeitpunkt = datetime.datetime.now()
benutzername = os.getlogin()
verzeichnis = os.getcwd()
rechnername = platform.node()
hostname = socket.getfqdn()
prozessortyp = platform.processor()
betriebssystem = platform.system()

#Einstellungen für das Log
logging.basicConfig(filename='monitoring.log', level=logging.DEBUG, format="%(asctime)s:%(levelname)s: %(message)s")
logging.debug("[--Statusbericht des Rechners--]")
logging.debug(f"Zeitpunkt des Berichts: {zeitpunkt}")
logging.debug(f"Aktueller Benutzer: {benutzername}")
logging.debug(f"Aktueller Verzeichnis: {verzeichnis}")
logging.debug(f"Rechnername: {rechnername}")
logging.debug(f"Hostname: {hostname}")
logging.debug(f"Prozessor: {prozessortyp}")
logging.debug(f"Betriebssystem: {betriebssystem}")
logging.debug(f"Python-Version: {sys.version}")
logging.debug("[--Status des Arbeitsspeichers und des Prozessors--]")

#Abfrage der grundlegenden Systemdaten
print ("[--Statusbericht des Rechners--]\n")
print ("Zeitpunkt des Berichts:",zeitpunkt)
print ("Aktueller Benutzer:",benutzername,"\nAktuelles Verzeichnis:",verzeichnis)
print ("Rechnername:",rechnername,"\nHostname:",hostname)
print ("Prozessor:",prozessortyp)
print ("Betriebssystem:",betriebssystem)
print ("Python-Version:",sys.version)

#Abfrage der Arbeitsspeicher- und CPU-Werte, Definition von 2 Variablen für psutil Funktionen
psu_ram = psu.virtual_memory().percent
psu_cpu = psu.cpu_percent()

ram_meldung = f"Aktuelle Arbeitsspeicher-Auslastung: {psu_ram}%"
print (ram_meldung)
cpu_meldung = f"Aktuelle Prozessor-Auslastung: {psu_cpu}%"
print (cpu_meldung)

print ("\n[--Meldungen:--]")

def ram_check():
    if psu_ram < 70:
       print ("Der Arbeitsspeicher ist in Ordnung")
       logging.debug("Der Arbeitsspeicher ist in Ordnung")
    else:
       print ("Der Arbeitsspeicher hat eine hohe Auslastung")
       logging.debug("Der Arbeitsspeicher hat eine hohe Auslastung")
def cpu_check():
    if psu_cpu < 70:
       print ("Der Prozessor ist in Ordnung")
       logging.debug("Der Prozessor ist in Ordnung")
    else:
       print ("Der Prozessor hat eine hohe Auslastung")
       logging.debug("Der Prozessor hat eine hohe Auslastung")
       
#Funktionen für die Überprüfung des Arbeitsspeichers und des Prozessors
ram_check()
cpu_check()